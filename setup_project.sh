# If dev directory doesnt exist, create it
if [ ! -d "./dev" ] 
then
    echo "dev virtualenv doesn't exist"
    python3 -m venv dev
    source ./dev/bin/activate
    pip install -r requirements.txt
    deactivate
fi

# If node_modules directory doesnt exist, create it
if [ ! -d "./node_modules" ]
then
    echo "nodemodules doesn't exist"
    npm install
fi

echo make sure to create virtualenv named "dev"
source ./dev/bin/activate
echo virtual env activated
