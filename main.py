import sys, getopt
import json
from datetime import datetime

def main():    
    json_obj = json.loads(sys.argv[1])    
    # print("args that python received: {}".format(sys.argv))    
    # print('function to call: ' + json_obj['function'])
    # print('param1: ' + json_obj['param1'])
    # print('param2: ' + json_obj['param2'])

    # end of main has to print 1 output that the server.js
    # will return to the client who sent the request
    print("Python script executed on: {}".format(datetime.now()))

if __name__ == "__main__":
    sys.exit(main())


