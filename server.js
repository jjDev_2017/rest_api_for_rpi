const express = require('express');
const bp = require('body-parser');
const app = express();
const { spawnSync } = require('child_process');

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

let PYTHON_SCRIPT_NAME = "main.py";
let PORT_NUM = 8000;

let runPythonScript = function(successCallback, failCallback, req, res) {

    const objEgress = { 
        function: 'FUNCTION-NAME-TO-CALL',
        param1: req.body.KEY_HERE,
        param2: 137
    }

    //spawnSync will block the main thread
    //it won't return until the child process has fully closed
    try {
        const resultObj = spawnSync('sudo', ['./dev/bin/python3', PYTHON_SCRIPT_NAME, JSON.stringify(objEgress)]);
        successCallback(resultObj.output.toString(), res)
    } catch(err) {
        failCallback(err.message, res)
    }
};

const successReceivedFromPython = (dataFromPython, res) => {
    console.log(`SUCCESS from python: ${dataFromPython.toString()}`);
    res.status(200).send(dataFromPython);
};

const failReceivedFromPython = (error, res) => {
    console.log(`ERROR from python: ${dataFromPython.toString()}`);
    res.status(400).json({
      message: 'ERROR - something went wrong UwU',
      error: error
    });
};

app.get('/', (req, res) => {
    res.write('Hello there\n');         
})

app.post('/param_test', (req, res)=>{
    // console.log(req.body.some-key);
    console.log(req.body);
    res.send("all good in the hood");
});

app.post('/python_script_test', (req, res) => {
    runPythonScript(successReceivedFromPython, failReceivedFromPython, req, res);
})



app.listen(PORT_NUM, () => console.log(`Application listening on port ${PORT_NUM}!`))